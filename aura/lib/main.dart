import 'dart:convert' as convert;
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_speedometer/flutter_speedometer.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decode/jwt_decode.dart';

import 'package:foreground_service/foreground_service.dart';

/// third rendered as a line.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:screen_loader/screen_loader.dart';

// Create storage
final secureStorage = new FlutterSecureStorage();
const BASE_URL = "http://somethingway.com";
// const BASE_URL = "http://68.183.107.0";

const Map<String, String> SENSOR_PROPERTIES = {
  'GR'              :   'Gas resistance',
  'HUM'             :   '%',
  'PSI'             :   'Hpa',
  'TMP'             :   '°C',
};

const SPEEDOMETER_BACKGROUND_COLOR = Colors.transparent;
const SERVICE_UUID = "0000dfb0-0000-1000-8000-00805f9b34fb";
const CHARACTERISTIC_UUID = "0000dfb1-0000-1000-8000-00805f9b34fb";
var IS_THERE_CONNECTED_DEVICE = false;

String jwtTokenInstance;
jwtToken()  async {
  if (jwtTokenInstance == null) {
    jwtTokenInstance = await secureStorage.read(key: "jwt_token");
    return jwtTokenInstance;
  }
  return jwtTokenInstance;
}

extractCustomerId(String jwtToken) {
  Map<String, dynamic> payload;
  var customerIdDecoded;
  try {
    payload = Jwt.parseJwt(jwtToken);
  } catch(e) {
    print(e);
  } finally {
    if (payload != null) {
      customerIdDecoded = payload["sub"];
    }
  }
  return customerIdDecoded;
}

void main() {
  runApp(MyApp());
}

var GLOBAL_DATA = {};

// use an async method so we can await
void maybeStartFGS() async {
  ///if the app was killed+relaunched, this function will be executed again
  ///but if the foreground service stayed alive,
  ///this does not need to be re-done
  if (!(await ForegroundService.foregroundServiceIsStarted())) {
    await ForegroundService.setServiceIntervalSeconds(5);



    // get connected device, and switch notify ONCE!
    var connectedDevices = await FlutterBlue.instance.connectedDevices;
    BluetoothDevice connectedDevice = (connectedDevices.length > 0) ? connectedDevices.first : null;

    if (connectedDevice != null) {

      IS_THERE_CONNECTED_DEVICE = true;

      // necessity of editMode is dubious (see function comments)
      await ForegroundService.notification.startEditMode();

      await ForegroundService.notification
          .setTitle("Aura is collecting your sensor data.");
      await ForegroundService.notification
          .setText("Sensor data is being send to the cloud.");

      await ForegroundService.notification.finishEditMode();




      BluetoothCharacteristic characteristic = await getNotifiableCharacteristic(connectedDevice);

      await characteristic.setNotifyValue(true);

      characteristic.value.listen((data) {

        var utf8DecodedData = convert.utf8.decode(data);
        var parsedData = _DeviceRealTimeDataState.gatherDataFromArduino(
            connectedDevice.id.toString(),
            utf8DecodedData
        );

        if (parsedData != null) {
          // Save to the database
          GLOBAL_DATA = parsedData;

          saveDeviceDataToDB(
              connectedDevice.id.toString(),
              parsedData
          );
        }
      });

      await ForegroundService.startForegroundService(foregroundServiceFunction);
      await ForegroundService.getWakeLock();
    } else {
      IS_THERE_CONNECTED_DEVICE = false;
    }
  }

  ///this exists solely in the main app/isolate,
  ///so needs to be redone after every app kill+relaunch
  await ForegroundService.setupIsolateCommunication((data) {
    debugPrint("main received: $data");
  });
}



void saveDeviceDataToDB(String deviceId, Map<String, String> parsedData) async {

  String jwt = await jwtToken();
  String customerId = extractCustomerId(jwt);
  String url = BASE_URL + "/customer/$customerId/devices/${deviceId}/store/";

  if (parsedData.isEmpty) return;

  http.post(
      url,
      headers: {
        "Content-Type" : "application/json",
        "Authorization" : jwt,
      },
      body: convert.jsonEncode(parsedData)
  ).then((response) {
    print("STATUS = ${response.statusCode}, ${response.body}");
  });
}

Map<String, String> getParseDataFromArduino(List<String> buffer) {

  var defaultMap = {
                    "GR"              :     "0",
                    "HUM"             :     "0",
                    "PSI"             :     "0",
                    "TMP"             :     "0",
                    "TS"              :     DateTime.now().millisecondsSinceEpoch.toString()
                  };

  if (buffer.length < 4 || buffer.length > 4) return null;

  for (var item in buffer) {
    if (!item.contains("_") && !item.contains(":")) {
      return null;
    }
  }

  print("BUFFER");
  print(buffer);

  for (var item in buffer) {
    var split = item.split(":");
    var theRealKey = split[0];
    var theRealValue = split[1].trim();

    if (theRealValue.contains("_")) {
      theRealValue = theRealValue.split("_")[0];
    } else if (theRealValue.lastIndexOf(".") == theRealValue.length - 1) {
      theRealValue = theRealValue.substring(0, split[1].trim().length - 1);
    } else {
      theRealValue = "0";
    }

    defaultMap[theRealKey] = double.parse(theRealValue).round().toString();
  }

  defaultMap["TS"] = DateTime.now().millisecondsSinceEpoch.toString();
  print(defaultMap);

  return defaultMap;
}

Future<BluetoothCharacteristic> getNotifiableCharacteristic(BluetoothDevice connectedDevice) async {
  BluetoothCharacteristic notifyableCharacteristic;

  var services = await connectedDevice.discoverServices();
  services.forEach((BluetoothService service) {
    if (service.uuid.toString().compareTo(SERVICE_UUID) == 0) {
      for (var characteristic in service.characteristics) {
        if (characteristic.uuid.toString().compareTo(CHARACTERISTIC_UUID) ==
            0) {
          if (notifyableCharacteristic == null) {
            notifyableCharacteristic = characteristic;
          }
        }
      }
    }
  });

  return notifyableCharacteristic;
}

Future<void> foregroundServiceFunction() async {
  // debugPrint("EXECUTING FOREGROUND SERVICE.");
  // debugPrint("The current time is: ${DateTime.now()}");
  // //ForegroundService.notification.setText("The time was: ${DateTime.now()}");
  //
  //
  // if (!ForegroundService.isIsolateCommunicationSetup) {
  //   ForegroundService.setupIsolateCommunication((data) {
  //     debugPrint("bg isolate received: $data");
  //   });
  // }
  //
  // ForegroundService.sendToPort("message from bg isolate");
}



class LoggedInChecker {

  static checkIfLoggedIn() async => await secureStorage.read(key: "jwt_token") != null;

  static redirectIfNotLoggedIn(context) async {
    bool isLoggedIn = await LoggedInChecker.checkIfLoggedIn();
    if (!isLoggedIn) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AuthenticatePage()),
        );
    }
  }
}

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class MyApp extends StatelessWidget {

  final appTitle = 'Aura';

  MyApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: appTitle),
      navigatorObservers: [routeObserver],
    );
  }
}

class RegisterUserRoute extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUserRoute> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Register Page"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 150,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child: Image.asset('asset/images/flutter-logo.png')),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email id as abc@gmail.com'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(

                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password'),
              ),
            ),
            FlatButton(
              onPressed: () {
                //TODO FORGOT PASSWORD SCREEN GOES HERE
              },
              child: Text(
                'Forgot Password',
                style: TextStyle(color: Colors.blue, fontSize: 15),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: FlatButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => MyApp()));
                },
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            SizedBox(
              height: 130,
            ),
            FlatButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => AuthenticatePage()));
              },
              child: Text(
                  'Already a user? Login'
              ),
            )
          ],
        ),
      ),
    );
  }
}

class AuthenticatePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _AuthenticatePageState();
}

class _AuthenticatePageState extends State<AuthenticatePage> with RouteAware {

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
    if (await LoggedInChecker.checkIfLoggedIn()) {
      Navigator.pop(context);
    }
  }

  @override
  void dispose() {
    // Clean up the controllers when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  _AuthenticatePageState();

  _postLogin(context) async {

    bool ok = false;
    String url = BASE_URL + "/auth/login";
    var response = await http.post(
        url,
      headers: {},
      body: {
        "email": emailController.text,
        "password": passwordController.text
      }
    );

    if (response.statusCode != 200) {
      print("ERROR ${response.statusCode}");
    } else {
      var jsonResponse = convert.jsonDecode(response.body);
      await secureStorage.write(
          key: 'jwt_token',
          value: jsonResponse["auth_token"]
      );
      ok = true;
    }

    return ok;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Login Page"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 100,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child: Text(
                      'Login',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 36,
                        fontWeight: FontWeight.bold
                      ),
                    )
                ),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: emailController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email id as abc@gmail.com'),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Field cannot be empty.';
                  }
                  if (!value.contains(new RegExp(r'^\S+@\S+$'))) {
                    return 'Field should contain valid email address.';
                  }
                  return value;
                }
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter a secure password'),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Field cannot be empty.';
                  }
                  return value;
                }
              ),
            ),
            FlatButton(
              onPressed: () {
                //TODO FORGOT PASSWORD SCREEN GOES HERE
                print("Not implemented.");
              },
              child: Text(
                'Forgot Password',
                style: TextStyle(color: Colors.blue, fontSize: 15),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: FlatButton(
                onPressed: () async {

                  // Send http request to the API
                  bool isLoggedIn = await _postLogin(context);
                  if (isLoggedIn) {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => MyDevicesRoute())
                    );
                  } else {
                    Scaffold.of(context).showSnackBar(
                        SnackBar(
                            content: Text(
                                "Something very bad happened. Please, don't beat the developer."
                            )
                        )
                    );
                  }
                },
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            SizedBox(
              height: 130,
            ),
            FlatButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => RegisterUserRoute()));
              },
              child: Text(
                'New User? Create Account'
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with RouteAware {

  String _appMessage = "";

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
    await LoggedInChecker.redirectIfNotLoggedIn(context);
  }

  void _scanForBluetoothDevices() {
    setState(() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => BluetoothDevicesRoute()),
      );
    });
  }

  void _toggleForegroundServiceOnOff() async {
    final fgsIsRunning = await ForegroundService.foregroundServiceIsStarted();
    String appMessage = "";

    if (fgsIsRunning) {
      await ForegroundService.stopForegroundService();
      appMessage = "Stopped foreground service.";

      // get connected device, and switch notify ONCE!
      var connectedDevices = await FlutterBlue.instance.connectedDevices;
      BluetoothDevice connectedDevice = (connectedDevices.length > 0) ? connectedDevices.first : null;

      if (connectedDevice != null) {
        BluetoothCharacteristic characteristic = await getNotifiableCharacteristic(connectedDevice);
        await characteristic.setNotifyValue(false);
      }

    } else {
      maybeStartFGS();
      appMessage = "No connected devices, could not start service.";
      Future.delayed(
          Duration(milliseconds: 1000)
      ).then((value) {
        if (IS_THERE_CONNECTED_DEVICE) {
          appMessage = "Started foreground service.";
          setState(() {
            _appMessage = appMessage;
          });
        }
      });
    }

    setState(() {
      _appMessage = appMessage;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            Icon(CupertinoIcons.flame_fill, size: 30)
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(CupertinoIcons.flame_fill, size: 100,),
            Align(
              alignment: Alignment.center,
              child: Text(
                  'Collecting air quality sensor data to make the world a better place.',
                  style: TextStyle(fontWeight: FontWeight.normal)
              ),
            ),
            Padding(padding: EdgeInsets.all(8.0)),
            Text(_appMessage, style: TextStyle(fontStyle: FontStyle.italic))

          ],
        ),
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Aura'),
              decoration: BoxDecoration(
                color: Colors.deepPurpleAccent,
              ),
            ),
            ListTile(
              title: Text('My devices'),
              onTap: () {

                Navigator.pop(context);

                // Navigate to the route
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyDevicesRoute()),
                );
              },
            ),
            ListTile(
              title: Text('Log out'),
              onTap: () async {
                await secureStorage.delete(key: "jwt_token");

                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AuthenticatePage()),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: Align(
        alignment: Alignment.bottomRight,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Column(
                  children: [
                    FloatingActionButton(
                      heroTag: "btn1",
                      onPressed: _scanForBluetoothDevices,
                      tooltip: 'Scan for bluetooth devices',
                      child: Icon(CupertinoIcons.search),
                    )
                  ],
                ),
                Column(
                  children: [
                    FloatingActionButton(
                      heroTag: "btn2",
                      child: Icon(CupertinoIcons.arrow_up_left),
                      onPressed: _toggleForegroundServiceOnOff,
                      tooltip: "Toggle Foreground Service On/Off",
                    )
                  ],
                ),
                Column(
                  children: [
                    FloatingActionButton(
                      heroTag: "btn3",
                      child: Icon(CupertinoIcons.text_badge_star),
                      onPressed: () async {
                        if (await ForegroundService.isBackgroundIsolateSetupComplete()) {
                          await ForegroundService.sendToPort("message from main");
                        } else {
                          debugPrint("bg isolate setup not yet complete");
                        }
                      },
                      tooltip: "Send test message to bg isolate from main app",
                    )
                  ],
                )
              ],
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

class _BluetoothDevicesState extends State<BluetoothDevicesRoute> with RouteAware {

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
    await LoggedInChecker.redirectIfNotLoggedIn(context);
  }

  StreamBuilder _bluetoothDevicesStream;
  var _connectedDevices = [];

  FlutterBlue flutterBlue = FlutterBlue.instance;

  _BluetoothDevicesState() {

    flutterBlue.startScan(
        timeout: Duration(seconds: 10)
    );

    _bluetoothDevicesStream = StreamBuilder(
      stream: flutterBlue.scanResults,
      initialData: [],
      builder: (BuildContext context, snapshot) {

        List<Container> scannedDevices = [];
        for (var scanResult in snapshot.data) {
          scannedDevices.add(
            Container(
              height: 80,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Text(
                          scanResult.device.name == ''
                              ? '(unknown device)'
                              : scanResult.device.name,
                          textAlign: TextAlign.left,
                        ),
                        Text(
                            scanResult.device.id.toString(),
                            textAlign: TextAlign.left
                        ),
                      ],
                    ),
                  ),
                  FlatButton(
                    color: Colors.blue,
                    disabledTextColor: Colors.grey,
                    child: _getConnectButtonText(scanResult.device),
                    onPressed: () async {

                      flutterBlue.stopScan();

                      if (this._isDeviceConnected(scanResult.device)) {
                        await scanResult.device.disconnect();

                        this._showSnackBarWithText(
                            context,
                            "The device was disconnected!"
                        );
                      } else {
                        try {
                          await scanResult.device.connect(timeout: Duration(seconds: 3));
                          this._showSnackBarWithText(
                              context,
                              "The device is successfully connected!"
                          );
                        }
                        catch (e) {
                          if (e.code == 'already_connected') {
                            this._showSnackBarWithText(
                                context,
                                "The device is already connected."
                            );
                          }
                          this._showSnackBarWithText(
                              context,
                              e.code
                          );
                        }
                        finally {

                          setState(() {
                            _connectedDevices.add(scanResult.device);
                          });

                          // store device if does not exist
                          await _storeDeviceIfNotExist(scanResult.device);

                          //maybeStartFGS();

                          Navigator.pop(context);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => MyDevicesRoute()),
                          );
                        }
                      }
                    },
                  ),
                ],
              ),
            ),
          );
        }

        return ListView(
          padding: EdgeInsets.zero,
          children: scannedDevices,
        );
      },
    );
  }


  _storeDeviceIfNotExist(BluetoothDevice device) async {

    String jwtTokenStr = await jwtToken();
    String customerId = extractCustomerId(jwtTokenStr);
    String url = BASE_URL + "/customer/$customerId/devices";
    Position userGeoLocation = await _getCurrentLocation();

    var response = await http.post(
        url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          "Authorization" : jwtTokenStr,
        },
        body: convert.jsonEncode({
          "name": device.name == '' ? '(unknown device)' : device.name,
          "description": "",
          "geolocation_data": {
            "lon": (userGeoLocation != null) ? userGeoLocation.longitude.toString() : "",
            "lat": (userGeoLocation != null) ? userGeoLocation.latitude.toString() : ""
          },
          "device_id": device.id.toString()
        }),
      encoding: Encoding.getByName("utf8")
    );

    print(response.reasonPhrase);
    print(response.request);
    print(response.toString());
    print(response);
    if (response.statusCode == 401) {
      secureStorage.delete(key: "jwt_token");
    }

    if (response.statusCode != 409) { // conflict
      // TODO: print meaningful message that the device was persisted to the database
    }
  }

  _getCurrentLocation() async {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    Position position;
    try {
      position = await geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
          .catchError((e) {
      });
    } catch (e) {
      print(e);
    }
    return position;
  }

  _showSnackBarWithText(BuildContext context, String text) {
    Scaffold.of(context).showSnackBar(
        SnackBar(content: Text(text))
    );
  }

  _getConnectButtonText(device) {

    String buttonText = 'Disconnect';

    if (!_isDeviceConnected(device)) {
      buttonText = 'Connect';
    }

    return Text(
      buttonText,
      style: TextStyle(color: Colors.white),
    );
  }

  _isDeviceConnected(device) {
    return _connectedDevices.indexOf(device) != -1;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text("Bluetooth devices list"),
    ),
    body: Center(
        child: _bluetoothDevicesStream
    ),
  );
}

class BluetoothDevicesRoute extends StatefulWidget {

  @override
  _BluetoothDevicesState createState() => _BluetoothDevicesState();
}

class MyDevicesRoute extends StatefulWidget {

  @override
  _MyDevicesRouteState createState() => _MyDevicesRouteState();
}

class _MyDevicesRouteState extends State<MyDevicesRoute> with RouteAware, ScreenLoader<MyDevicesRoute> {

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
    await LoggedInChecker.redirectIfNotLoggedIn(context);
  }

  Map<String, bool> connectedDevicesStates = {};

  List<Widget> _connectedDevices = [];

  @override
  Future<void> initState() {
    super.initState();

    _getMyDevices().then((myDevices) async {

      List<Widget> localList = [];
      for (var device in myDevices) {
        //var isConnected = await this._isDeviceConnected(device['device_id']);

          await this._isDeviceConnected(device['device_id']).then((isConnected) {
          var deviceRow = _getConnectedDeviceRow(device, isConnected);
          localList.add(deviceRow);
        });
      }

      Future.delayed(Duration(milliseconds: 200)).then((value) {
        setState(() {
          _connectedDevices = localList;
        });
      });
    });
  }

  _MyDevicesRouteState();

  _isDeviceStateConnected(theConnectedDevice) {
    if (connectedDevicesStates.isEmpty) return false;
    return connectedDevicesStates[
      theConnectedDevice['device_id']
    ];
  }

  _getConnectedDeviceRow(connectedDevice, isConnected) {

    //bool isConnected = await this._isDeviceConnected(connectedDevice['device_id']);
    connectedDevicesStates.putIfAbsent(connectedDevice['device_id'], () => isConnected);

    return Container(
      height: 150,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                                connectedDevice['name'] == '' ? '(unknown device)'
                                    : connectedDevice['name'],
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold
                                )
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                                connectedDevice['device_id'].toString(),
                                textAlign: TextAlign.center
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                                connectedDevice['geolocation_data'].toString(),
                                textAlign: TextAlign.center
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        FlatButton(
                          color: Colors.blue,
                          disabledTextColor: Colors.grey,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                  (_isDeviceStateConnected(connectedDevice)) ?
                                  "Disconnect" : "Connect"
                              )
                            ],
                          ),
                          onPressed: () {
                              if (_isDeviceStateConnected(connectedDevice)) {

                                _disconnectAlreadyConnectedDevice(connectedDevice['device_id']);

                                Future.delayed(Duration(milliseconds: 500)).then((value) => {
                                  setState(() {})
                                });
                                // Scaffold.of(context).showSnackBar(
                                //     SnackBar(content: Text(
                                //         "The device was disconnected.")
                                //     )
                                // );
                              } else {

                                try {

                                  _connectDeviceById(connectedDevice['device_id']);
                                  Future.delayed(Duration(milliseconds: 500)).then((value) => {
                                    setState(() {})
                                  });

                                  // Scaffold.of(context).showSnackBar(
                                  //     SnackBar(content: Text(
                                  //         "The device is successfully connected!")
                                  //     )
                                  // );
                                } catch (e) {
                                  //print(e);
                                  // Scaffold.of(context).showSnackBar(
                                  //     SnackBar(content: Text(e.code))
                                  // );
                                }
                              }
                          },
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        FlatButton(
                          color: Colors.blue,
                          disabledTextColor: Colors.grey,
                          child: Icon(CupertinoIcons.timer),
                          onPressed: () async {

                            if (!_isDeviceStateConnected(connectedDevice)) {
                              return false;
                            }

                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>
                                  DeviceRealTimeDataRoute(
                                      deviceId: connectedDevice['device_id']
                                  )),
                            );
                          },
                        )
                      ],
                    ),
                    Column(
                      children: [
                        FlatButton(
                          color: Colors.blue,
                          disabledTextColor: Colors.grey,
                          child: Icon(CupertinoIcons.chart_bar_alt_fill),
                          onPressed: () async {

                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>
                                  DeviceHistoricDataRoute(
                                      deviceId: connectedDevice['device_id']
                                  )),
                            );
                          },
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _getPlaceholderForMyDevices() {
    this._connectedDevices = [
      Container(
        height: 150,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Text(
                    "No connected devices.\nGo to home page and click scan to connect your devices.",
                    textAlign: TextAlign.left,
                  )
                ],
              ),
            ),
            FlatButton(
              color: Colors.blue,
              disabledTextColor: Colors.grey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                      "Go home",
                      textAlign: TextAlign.left
                  )
                ],
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePage(
                    title: "Aura",
                  )),
                );
              },
            ),
          ],
        ),
      )
    ];
  }

  Future<List<dynamic>> _getMyDevices() async {
    String jwt = await jwtToken();
    String customerId = extractCustomerId(jwt);
    String url = BASE_URL + "/customer/$customerId/devices";

    var response = await http.get(
      url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": jwt,
      },
    );

    return convert.jsonDecode(response.body);
  }

  _connectDeviceById(deviceId) {
    FlutterBlue.instance.startScan(timeout: Duration(seconds: 5));

    FlutterBlue.instance.scanResults.listen((List<ScanResult> list) async {
      if (list.length == 0) {
        return false;
      }

      BluetoothDevice device;
      try {
        device = list.firstWhere((ScanResult scanResult) {
          return (scanResult.device.id.toString().compareTo(deviceId) == 0);
        }).device;
      }
      catch (e) {}

      if (device != null) {
        FlutterBlue.instance.stopScan();

        try {
          // await _disconnectAlreadyConnectedDevice(device.id.toString());


          device.connect(timeout: Duration(seconds: 5)).then((value) {
            setState(() {
              connectedDevicesStates[deviceId] = true;
            });

            getNotifiableCharacteristic(device).then((characteristic) {
              if (!characteristic.isNotifying) {
                characteristic.setNotifyValue(true);
              }
            });
          });
        } catch (e) {}
      }
    });
  }

  _disconnectAlreadyConnectedDevice(String deviceId) async {

    var connectedDevices = await FlutterBlue.instance.connectedDevices;
    var device;
    try {
      device = connectedDevices.firstWhere((device) => device.id.toString().compareTo(deviceId) == 0);
    } catch(e) {}

    if (device != null) {

      getNotifiableCharacteristic(device).then((characteristic) { // first turn off notification descriptor

        if (characteristic.isNotifying) {
          characteristic.setNotifyValue(false);
        }

        device.disconnect(); // then disconnect
        Map<String, bool> localMap = connectedDevicesStates;
        localMap[deviceId] = false;

        setState(() {
          connectedDevicesStates = localMap;
        });
      });
    }
  }

  Future<bool> _isDeviceConnected(wantedDeviceId) async {

    var isConnected = false;
    var connectedDevices = await FlutterBlue.instance.connectedDevices;
    connectedDevices.forEach((BluetoothDevice connectedDevice) {

      print('ConnectedDevice ${connectedDevice.id.toString()}');
      print('Other device $wantedDeviceId');

      if (wantedDeviceId.toString().compareTo(connectedDevice.id.toString()) == 0) {
        isConnected = true;
        return isConnected;
      }
    });

    return isConnected;
  }

  @override
  loadingBgBlur() => 10.0;

  @override
  loader() {
    // here any widget would do
    return AlertDialog(
      title: Text('Wait.. Loading data..'),
    );
  }

  @override
  Widget screen(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("My Devices"),
        ),
        body: Center(
            child: ListView(
                padding: EdgeInsets.all(10),
                children: _connectedDevices
            )
        )
    );
  }
}

class DeviceRealTimeDataRoute extends StatefulWidget {

  final deviceId;
  DeviceRealTimeDataRoute({Key key, @required this.deviceId}) : super(key: key);

  @override
  _DeviceRealTimeDataState createState() => _DeviceRealTimeDataState(deviceId);
}

class _DeviceRealTimeDataState extends State<DeviceRealTimeDataRoute> with RouteAware {

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
    await LoggedInChecker.redirectIfNotLoggedIn(context);
  }

  BluetoothCharacteristic notifyCharacteristic;
  
  static Map<String, String> _deviceData = {
    "GR"              :     "0",
    "HUM"             :     "0",
    "PSI"             :     "0",
    "TMP"             :     "0",
    "TS"              :     "0"
  };

  static List<String> buffer = [];

  setDevices(parsedData) {
    setState(() {
      _deviceData = parsedData;
    });
  }

  _DeviceRealTimeDataState(String deviceId) {

    FlutterBlue.instance.connectedDevices.then((List<BluetoothDevice> connectedDevices) => {

      if (connectedDevices.length == 0) {
        // The target device was disconnected or something very bad just happened.
        // TODO: add snackbar
      },

      connectedDevices.forEach((BluetoothDevice device) {
        if (device.id.toString().compareTo(deviceId.toString()) == 0) {

          device.state.listen((BluetoothDeviceState state) async {

            if (state == BluetoothDeviceState.connecting) {
              print("Connecting...");
              // TODO: add snackbar
            }

            if (state == BluetoothDeviceState.connected) {
              var services = await device.discoverServices();
              for (var service in services) {
                if (service.uuid.toString().compareTo(SERVICE_UUID) == 0) {
                  for (var characteristic in service.characteristics) {
                    if (characteristic.uuid.toString().compareTo(
                        CHARACTERISTIC_UUID) == 0) {

                      if (!characteristic.isNotifying) {
                        print("It's not notifying.");
                        await characteristic.setNotifyValue(true);


                        characteristic.value.listen((data) {
                          var utf8DecodedData = convert.utf8.decode(data);
                          Map<String, String> parsedData = gatherDataFromArduino(
                              deviceId, utf8DecodedData);

                          if (parsedData != null) {
                            // Save to the database
                            saveDeviceDataToDB(deviceId, parsedData);

                            if (mounted) {
                              setState(() {
                                print('UPDATE');
                                print('parsedData');
                                print(parsedData);
                                _deviceData = parsedData;
                              });
                            }
                          }
                        });
                      }

                      // setState(() {
                      //   _deviceData = GLOBAL_DATA;
                      // });
                      //await ForegroundService.stopForegroundService();



                    }
                  }
                }
              }
            }
          });
        }
      })
    });
  }

  static String removeLastCharacter(String str) {

    String result;

    if ((str != null) && (str.length > 0)) {
      result = str.substring(0, str.length - 1);
    }

    return result;
  }

  static formatIncomingMessage(String arduinoChunkDecoded) {

    var trimmedString = arduinoChunkDecoded.replaceAll(new RegExp(r"\s\b|\b\s"), "");

    if (trimmedString == null) {
      return null;
    }

    trimmedString = trimmedString.replaceAll("\n", "");
    if (trimmedString == null) {
      return null;
    }

    trimmedString = trimmedString.replaceAll("\r", "");

    if (trimmedString == null) {
      return null;
    }

    trimmedString = removeLastCharacter(trimmedString);

    if (trimmedString == null) {
      return null;
    }

    trimmedString = trimmedString.replaceAll("\x00", "");

    if (trimmedString == null) {
      return null;
    }

    if (trimmedString == "") {
      return null;
    }
    return trimmedString;
  }

  static gatherDataFromArduino(String deviceId, String arduinoChunkDecoded) {

    print("=== Arduino Chunk Decoded ===");
    print(arduinoChunkDecoded);

    arduinoChunkDecoded = formatIncomingMessage(arduinoChunkDecoded);
    if (arduinoChunkDecoded != null) {
      if (arduinoChunkDecoded == "START:1_") {
        buffer.add(arduinoChunkDecoded);
      } else if (arduinoChunkDecoded == "END:1_") {
        buffer.remove("START:1_");
        Map<String, String> parsedData = getParseDataFromArduino(buffer);
        buffer = [];
        return parsedData;
      } else {
        buffer.add(arduinoChunkDecoded);
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    FlutterBlue.instance.state.listen((BluetoothState state) {
      if (state == BluetoothState.off) {
        Scaffold.of(context).showSnackBar(
            SnackBar(
                content: Text(
                    "Bluetooth is Off. Please, turn it on."
                )
            )
        );
      }
    });

    return Scaffold(
      appBar: AppBar(
        title: Text("Device Realtime Data"),
      ),
      body: Builder(
        builder: (BuildContext context) {
          Row firstRow = Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: Column(
                      children: <Widget>[
                        Speedometer(
                          backgroundColor: Colors.grey,
                          meterColor: Colors.lightGreenAccent,
                          warningColor: Colors.orange,
                          kimColor: Colors.black,
                          displayNumericStyle: TextStyle(
                            fontFamily: 'Digital-Display',
                            color: Colors.white,
                            fontSize: 22
                          ),
                          //displayText: '°C',
                          displayTextStyle: TextStyle(color: Colors.white, fontSize: 16),

                          //backgroundColor: SPEEDOMETER_BACKGROUND_COLOR,
                          minValue: 0,
                          maxValue: 100,
                          currentValue: int.parse((_deviceData != null) ? _deviceData["GR"] : 0),
                          warningValue: 40,
                          displayText: 'Gas resistance',
                        ),
                      ]
                  )
                ),
                Expanded(child: Column(
                    children: <Widget>[
                      Speedometer(
                        backgroundColor: Colors.grey,
                        meterColor: Colors.lightGreenAccent,
                        warningColor: Colors.orange,
                        kimColor: Colors.black,
                        displayNumericStyle: TextStyle(
                            fontFamily: 'Digital-Display',
                            color: Colors.white,
                            fontSize: 22
                        ),
                        //displayText: '°C',
                        displayTextStyle: TextStyle(color: Colors.white, fontSize: 16),
                        minValue: 0,
                        maxValue: 100,
                        currentValue: int.parse((_deviceData != null) ? _deviceData["HUM"] : 0),
                        warningValue: 50,
                        displayText: '%',
                      )
                    ]
                )),
              ]
          );
          
          Row secondRow = Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(child: Column(
                    children: <Widget>[
                      Speedometer(
                        backgroundColor: Colors.grey,
                        meterColor: Colors.lightGreenAccent,
                        warningColor: Colors.orange,
                        kimColor: Colors.black,
                        displayNumericStyle: TextStyle(
                            fontFamily: 'Digital-Display',
                            color: Colors.white,
                            fontSize: 22
                        ),
                        //displayText: '°C',
                        displayTextStyle: TextStyle(color: Colors.white, fontSize: 16),

                        minValue: 0,
                        maxValue: 100,
                        currentValue: int.parse((_deviceData != null) ? _deviceData["PSI"] : 0),
                        warningValue: 40,
                        displayText: 'Hpa',
                      )
                    ]
                )),
                Expanded(child: Column(
                    children: <Widget>[
                      Speedometer(
                        backgroundColor: Colors.grey,
                        meterColor: Colors.lightGreenAccent,
                        warningColor: Colors.orange,
                        kimColor: Colors.black,
                        displayNumericStyle: TextStyle(
                            fontFamily: 'Digital-Display',
                            color: Colors.white,
                            fontSize: 22
                        ),
                        //displayText: '°C',
                        displayTextStyle: TextStyle(color: Colors.white, fontSize: 16),
                        minValue: 0,
                        maxValue: 100,
                        currentValue: int.parse((_deviceData != null)? _deviceData["TMP"] : 0),
                        warningValue: 40,
                        displayText: '°C',
                      )
                    ]
                )),
              ]
          );

          return ListView(
              children: <Widget>[
                firstRow,
                secondRow
              ]
          );
        },
      ),
    );
  }
}

class DeviceHistoricDataRoute extends StatefulWidget {

  final deviceId;
  DeviceHistoricDataRoute({
    Key key,
    @required this.deviceId
  }) : super(key: key);

  @override
  _DeviceHistoricDataState createState() => _DeviceHistoricDataState(
      deviceId
  );
}

class _DeviceHistoricDataState extends State<DeviceHistoricDataRoute> with RouteAware {

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
    await LoggedInChecker.redirectIfNotLoggedIn(context);
  }

  List<DeviceData> _deviceData = [];
  
  List<Widget> _allCharts = [];

  var _deviceId;

  _getDeviceDataUrl(customerId, deviceId) => BASE_URL + "/customer/$customerId/devices/$deviceId/fetch/";

  _DeviceHistoricDataState(String deviceId) {
    this._deviceId = deviceId;

    this._getDeviceData(this._deviceId);

    Future.delayed(Duration(seconds: 1)).then((value) {

      List<Widget> localList = [];

      SENSOR_PROPERTIES.forEach((property, unit) {
        localList.add(_getChartPerProperty(property, unit));
      });

      setState(() {
        this._allCharts = localList;
      });
    });
  }

  _getQueryPeriod() {
    var today = new DateTime.now();
    var yesterday = today.subtract(new Duration(days: 1)); // all my troubles seemed so far away
    return {
      "from": yesterday.microsecondsSinceEpoch.toString(),
      "to": today.millisecondsSinceEpoch.toString()
    };
  }

  _getDeviceData(deviceId) async {

    String jwt = await jwtToken();
    String customerId = extractCustomerId(jwt);
    String url = _getDeviceDataUrl(customerId, deviceId.toString());
    var response = await http.post(
        url,
        headers: {
          "Content-Type" : "application/json",
          "Authorization" : jwt,
        },
        body: convert.jsonEncode(_getQueryPeriod()));

    if (response.statusCode != 200) {
      print("ERROR ${response.statusCode}");
      if (response.statusCode == 401) {
        secureStorage.delete(key: "jwt_token");
      }
    } else {

      List<dynamic> jsonData = convert.jsonDecode(response.body);
      print(jsonData);
      jsonData.forEach((dynamic element) {
        _deviceData.add(
            DeviceData(
                DateTime.fromMicrosecondsSinceEpoch(int.parse(element["TS"])),
                num.parse(element["HUM"]),
                num.parse(element["PSI"]),
                num.parse(element["TMP"]),
                num.parse(element["GR"])
            )
        );
        print(_deviceData);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print(_allCharts.length);

    return Scaffold(
      appBar: AppBar(
        title: Text("Device Historical Data"),
      ),
      body: Builder(
        builder: (BuildContext context) {
          return ListView(
            padding: EdgeInsets.all(10),
              children: _allCharts
          );
        },
      ),
    );
  }

  _getChartPerProperty(String property, String unit) {

    final element = Row(
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                                property.toUpperCase(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold
                                )
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                                unit,
                                textAlign: TextAlign.center
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        child: Column(
                          children: [
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 1,
                              height: MediaQuery.of(context).size.width * 1,
                              child: TimeSeriesBar.withData(this._deviceData, property, unit),
                            )
                          ],
                        ))
                  ],
                )
              ],
            ),
          ),
        ],
      );

    return element;
  }
}

class TimeSeriesBar extends StatelessWidget {

  final List<charts.Series<DeviceData, DateTime>> seriesList;
  final bool animate;

  TimeSeriesBar(this.seriesList, {this.animate});

  factory TimeSeriesBar.withData(List<DeviceData> deviceDataList, String property, String unit) {
    return new TimeSeriesBar(
      _createData(deviceDataList, property, unit),
      animate: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(
      seriesList,
      animate: animate,
      animationDuration: Duration(milliseconds: 300),
      defaultRenderer: new charts.BarRendererConfig<DateTime>(),
      defaultInteractions: true,
      // behaviors: [new charts.SelectNearest(), new charts.DomainHighlighter()],
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<DeviceData, DateTime>> _createData(List<DeviceData> deviceDataList, String property, String unit) {
    final data = deviceDataList;

    return [
      new charts.Series<DeviceData, DateTime>(
        id: property.toUpperCase(),
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (DeviceData deviceData, _) => deviceData.timestamp,
        measureFn: (DeviceData deviceData, _) => deviceData.get(property.toLowerCase()),
        data: data,
      )
    ];
  }
}

class DeviceData {

  final DateTime timestamp;
  final hum;
  final psi;
  final tmp;
  final gr;

  DeviceData(this.timestamp, this.hum, this.psi, this.tmp, this.gr);

  Map<String, dynamic> _toMap() {
    return {
      'timestamp'   : timestamp,
      'hum'         : hum,
      'psi'         : psi,
      'tmp'         : tmp,
      'gr'          : gr
    };
  }

  dynamic get(String propertyName) {
    var _mapRep = _toMap();
    if (_mapRep.containsKey(propertyName)) {
      return _mapRep[propertyName];
    }
    throw ArgumentError('Property not found');
  }
}